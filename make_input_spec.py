#!/usr/bin/env python3

"""
script to build an input specification file for Wei's network
"""

from h5py import File
import json
import argparse as ap
from sys import stdout, stderr
import re
from collections import defaultdict

from common import variables


def get_args():
    parser = ap.ArgumentParser(description=__doc__)
    parser.add_argument('weights_file')
    return parser.parse_args()

gev_units = {'mass', 'pt'}
renaming = [
    (
        re.compile('DL1rT_(?P<prob>p[ucb])_(?P<num>[0-9]+)'),
        'DL1rT_{prob}')
    ]
def get_jet_number(name):
    for regex, tmp in renaming:
        matches = regex.match(name)
        if matches:
            num = int(matches.group('num'))
            return num - 1, tmp.format(prob=matches.group('prob'))
    return -1, name

rename = {n:f'subjet{n}' for n in range(3)}
rename[-1] = 'fatjet'

def run():
    args = get_args()
    with File(args.weights_file,'r') as h5file:
        means = h5file['mean'][:,0:-1].flatten()
        stds = h5file['std'][:,0:-1].flatten()
    var_dict = defaultdict(list)
    for name, mean, std in zip(variables[7:], means, stds):
        if name in gev_units:
            stderr.write(f'{name} is assumed GeV, multiplying by 1000\n')
            mean *= 1e3
            std *= 1e3

        jet_num, new_name = get_jet_number(name)
        var_dict[jet_num].append(
            {'name': new_name, 'scale': 1/std, 'offset': -mean,
             'default': mean})

    var_list = sorted([(n,l) for n,l in var_dict.items()])
    output = {
        "input_sequences": [],
        "inputs": [ {"name": rename[n], "variables":v} for n,v in var_list],
        "outputs": [
            {
                "labels": ["QCD", "Higgs", "Top"],
                "name": "JKDL1r"
            }
        ]
    }
    json.dump(output, stdout, indent=2)

if __name__ == '__main__':
    run()
