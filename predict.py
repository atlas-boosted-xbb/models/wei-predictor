#!/usr/bin/env python3

"""
Simple script (adapted from Wei's script) that will dump the
prediction for a few events.
"""

import argparse
import os,glob,h5py
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
parser.add_argument("--model", dest='model',  default="", help="model")
args = parser.parse_args()
import tensorflow as tf
from keras import backend as K
import keras
from keras.models import Model
from keras.layers import Input
import numpy as np
import pandas as pd

from common import variables as not_sure_what_these_are

#Features
features1=['mcEventWeight','eventNumber','mass','pt','eta'] #0-9
features2=['DL1rT_pu', 'DL1rT_pc', 'DL1rT_pb', 'relativeDeltaRToVRJet']
list3={}
list4={}
list5={}
for i in features2:
      list3[i]=i+"_1"
      list4[i]=i+"_2"
      list5[i]=i+"_3"

#Flatten
paths=args.path
h0=pd.read_hdf(paths,"fat_jet")[features1]
h1=pd.read_hdf(paths,"subjet_VRGhostTag_1")[features2]
h1.rename(columns=list3,inplace=True)
h2=pd.read_hdf(paths,"subjet_VRGhostTag_2")[features2]
h2.rename(columns=list4,inplace=True)
h3=pd.read_hdf(paths,"subjet_VRGhostTag_3")[features2]
h3.rename(columns=list5,inplace=True)
h=pd.concat([h0,h1,h2,h3], axis=1)
h["dsid"]=0
h["pt"] = (h["pt"]/1000.0).astype("float64")
h["mass"] = (h["mass"]/1000.0).astype("float64")
variables = not_sure_what_these_are
Data=h[variables]
Data=Data.values

#DL1rBaseline
f=0.018
DL1r_baseline=np.stack([Data[:,11:12]/((1-f)*Data[:,9:10]+f*Data[:,10:11]), Data[:,14:15]/((1-f)*Data[:,12:13]+f*Data[:,13:14])], axis=1).min(axis=1)
invalid = np.isnan(DL1r_baseline) | np.isinf(DL1r_baseline)
DL1r_baseline[invalid] = 1e-15
DL1rBaseline=np.log(np.clip(DL1r_baseline, 1e-30, 1e30))
#print DL1rBaseline.shape,DL1rBaseline[100:140,:]

#binned_dl1r
DL1rDisc1=Data[:,11:12]/((1-f)*Data[:,9:10]+f*Data[:,10:11])
DL1rDisc2=Data[:,14:15]/((1-f)*Data[:,12:13]+f*Data[:,13:14])
DL1rDisc3=Data[:,17:18]/((1-f)*Data[:,15:16]+f*Data[:,16:17])
DL1rDisc=np.hstack((DL1rDisc1,DL1rDisc2,DL1rDisc3))

#Scaling
info=np.hstack((Data[:,0:9],DL1rBaseline))
train=np.hstack((Data[:,7:18],DL1rDisc))
meanFile=h5py.File("meanstd.h5","r")
mean_vector=meanFile.get("mean")
std_vector=meanFile.get("std")
train=(train-mean_vector)/std_vector
train=np.nan_to_num(train)

#Predict
JKDL1r="WeiAdmStd_JKDL1r.h5"
JKDL1r_pre = keras.models.load_model(JKDL1r)

JK_data=train[:,0:2]
DL1r_data=train[:,2:11]
JKDL1r_data=np.hstack((JK_data,DL1r_data))

predictions_JKDL1r=JKDL1r_pre.predict(JKDL1r_data)

print(predictions_JKDL1r[0])

with h5py.File(paths, 'r') as h5file:
    print(h5file['fat_jet'][0,'JKDL1r_Higgs','JKDL1r_QCD', 'JKDL1r_Top'])




