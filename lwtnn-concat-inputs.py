#!/usr/bin/env python3

"""
Splits the inputs of a lwtnn model.

With no second argument, will read from standard input.
"""
_epe="with no arguments, will read old model from standard inputs"


import json
import sys
import pathlib

from argparse import ArgumentParser

def get_args():
    parser = ArgumentParser(description=__doc__, epilog=_epe)
    parser.add_argument('input_file', help='new inputs json file')
    parser.add_argument(
        'model_file', nargs='?', help='old model file',
        type=pathlib.PosixPath)
    return parser.parse_args()

def count_vars(inputs):
    return sum([len(n['variables']) for n in inputs])

def make_node(inspec, index):
    n_variables = len(inspec['variables'])
    return {"size":n_variables, "sources":[index], "type":"input"}

def main():
    args = get_args()
    if args.model_file:
        with open(args.model_file) as mfile:
            old_nn = json.load(mfile)
    else:
        old_nn = json.load(sys.stdin)

    with open(args.input_file) as ifile:
        new_inputs = json.load(ifile)

    ni = new_inputs['inputs']
    n_inputs = count_vars(ni)
    if count_vars(old_nn['inputs']) != n_inputs:
        sys.exit("number of input mismatch")

    new_nn = old_nn.copy()
    new_nn['inputs'] = ni

    new_nodes = [make_node(s,i) for i,s in enumerate(ni)]
    concat_node = {
        "sources":list(range(len(new_nodes))),
        "type": "concatenate"
    }
    new_nodes.append(concat_node)
    n_input_nodes = len(ni)
    for node in old_nn['nodes'][1:]:
        new_sources = [s+n_input_nodes for s in node['sources']]
        node['sources'] = new_sources
        new_nodes.append(node)
    new_nn['nodes'] = new_nodes

    nodes_by_nlables = {len(n['labels']):n for n in new_inputs['outputs']}
    assert len(nodes_by_nlables) == len(new_inputs['outputs'])
    new_outputs = {}
    for out in new_nn['outputs'].values():
        out_len = len(out['labels'])
        out['node_index'] += n_input_nodes
        out['labels'] = nodes_by_nlables[out_len]['labels']
        new_outputs[nodes_by_nlables[out_len]['name']] = out

    new_nn['outputs'] = new_outputs

    json.dump(new_nn, sys.stdout)

if __name__ == '__main__':
    main()
