#!/usr/bin/env python3


from keras.layers import Concatenate, Input
from keras import Model
from sys import stdout

fat=Input((2,))
subjets = []
for j in range(3):
    subjets.append(Input((3,)))
concat = Concatenate()([fat] + subjets)
model = Model(inputs=[fat] + subjets, outputs=[concat])
print(model.to_json(indent=2))
