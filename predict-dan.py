#!/usr/bin/env python3

from argparse import ArgumentParser
import numpy as np
from h5py import File
import json

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('arch')
    parser.add_argument('weights')
    parser.add_argument('variables')
    parser.add_argument('inputs')
    return parser.parse_args()

def get_preproc_and_labels(varspec):
    inputs = []
    for in_node in varspec['inputs']:
        vl = in_node['variables']
        nodedict = {
            'names': [v['name'] for v in vl],
            'offsets': np.array([v['offset'] for v in vl],
                                ndmin=2, dtype='f4'),
            'scales': np.array([v['scale'] for v in vl],
                               ndmin=2,dtype='f4')
        }
        inputs.append((in_node['name'], nodedict))
    return inputs

input_map = {f'subjet{n}':f'subjet_VRGhostTag_{n+1}' for n in range(3)}
input_map['fatjet'] = 'fat_jet'

def run():
    args = get_args()
    from keras.models import model_from_json
    with open(args.arch) as json_config:
        model = model_from_json(json_config.read())
    model.load_weights(args.weights)

    with open(args.variables) as vfile:
        input_spec = get_preproc_and_labels(json.load(vfile))
    all_inputs = []
    with File(args.inputs,'r') as input_file:
        for nodename, nd in input_spec:
            seltup = tuple(nd['names']) + (slice(0,1),)
            ni = input_file[input_map[nodename]][seltup]
            ftype = [(n, float) for n in ni.dtype.names]
            flat = ni.astype(ftype).view(float).reshape(ni.shape + (-1,))
            scaled = (flat + nd['offsets'])*nd['scales']
            denaned = np.nan_to_num(scaled)
            all_inputs.append(denaned)
    print(model.predict(all_inputs))



if __name__ == '__main__':
    run()
